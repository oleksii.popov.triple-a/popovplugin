<?php

namespace PopovPlugin\Command;

use PopovPlugin\Migration\MigrationManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MigrateCommand extends Command implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var MigrationManager */
    private $migrationManager;

    public function __construct(MigrationManager $migrationManager)
    {
        $this->migrationManager = $migrationManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('aaa:popov:migrate')
            ->setDescription('Executes SQL migrations if needed.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $executedMigrationsCount = $this->migrationManager->migrate();

        if ($executedMigrationsCount > 0) {
            $output->writeln("<info>$executedMigrationsCount migrations were executed.</info>");
        } else {
            $output->writeln("<info>No migrations executed.</info>");
        }
    }
}