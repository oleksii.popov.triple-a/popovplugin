<?php

namespace PopovPlugin;

use PopovPlugin\Migration\MigrationManager;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UpdateContext;

class PopovPlugin extends Plugin
{
    public function install(InstallContext $context)
    {
        $updateContext = new UpdateContext(
            $context->getPlugin(),
            Shopware()->Container()->get('config')->get('version'),
            '0.0.0',
            $context->getCurrentVersion()
        );

        $this->update($updateContext);

        $context->scheduleClearCache([InstallContext::CACHE_TAG_PROXY]);
    }

    public function update(UpdateContext $context)
    {
        $migrationManager = new MigrationManager();
        $migrationManager->migrate();
    }
}