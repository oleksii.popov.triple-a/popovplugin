<?php

declare(strict_types=1);

namespace PopovPlugin\Migration;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Migration_0_0_1 implements MigrationInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getVersion(): string
    {
        return '0.0.1';
    }

    public function migrate(): void
    {
        $this->container->get('db')
            ->query(
                "
                CREATE TABLE IF NOT EXISTS `aaa_articles_rating` (
                    `article_id` int(11) NOT NULL,
                    `rating` int(11) DEFAULT 0,
                    PRIMARY KEY (`article_id`),
                    UNIQUE (`article_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

                ALTER TABLE s_articles_attributes
	                ADD COLUMN IF NOT EXISTS aaa_weather ENUM('summer','autumn','winter','spring') NULL;
            "
            );
    }
}