<?php

namespace PopovPlugin\Migration;

use Exception;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MigrationManager
{
    use ContainerAwareTrait;

    protected $migrationStorage;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        $this->container = Shopware()->Container();
        $this->migrationStorage = new \SplObjectStorage();
    }

    public function migrate()
    {
        $this->autoSearchMigrations();

        /** @var MigrationInterface[] $migrations */
        $migrations = iterator_to_array($this->migrationStorage);

        // Do not apply migrations prior current version
        $migrations = array_filter(
            $migrations,
            function (MigrationInterface $migration) {
                return version_compare($migration->getVersion(), $this->getCurrentVersion(), '>');
            }
        );

        // Sort migrations in chronological order
        usort(
            $migrations,
            function (MigrationInterface $a, MigrationInterface $b) {
                return version_compare($a->getVersion(), $b->getVersion());
            }
        );

        foreach ($migrations as $migration) {
            $migration->migrate();
        }
    }

    public function autoSearchMigrations()
    {
        $possibleMigrationFiles = glob(__DIR__ . '/Migration_*_*_*.php');

        foreach ($possibleMigrationFiles as $file) {
            $className = __NAMESPACE__ . '\\' . basename($file, '.php');

            if (in_array(MigrationInterface::class, class_implements($className), true)) {
                $migration = new $className();
                if ($migration instanceof ContainerAwareInterface) {
                    $migration->setContainer($this->container);
                }

                $this->addMigration($migration);
            }
        }
    }

    public function addMigration(MigrationInterface $migration)
    {
        $this->migrationStorage->attach($migration);
    }

    private function getCurrentVersion(): string
    {
        $currentVersion = $this->container->get('db')
            ->query('SELECT version FROM aaa_migrations ORDER BY executed_at DESC LIMIT 1')
            ->fetch(\PDO::FETCH_COLUMN);

        return $currentVersion ?: '0.0.0';
    }
}
