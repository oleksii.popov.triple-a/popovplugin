<?php

namespace PopovPlugin\Migration;

interface MigrationInterface
{
    public function getVersion(): string;
    public function migrate(): void;
}
