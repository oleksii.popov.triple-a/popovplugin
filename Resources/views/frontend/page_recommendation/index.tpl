{extends file="frontend/index/index.tpl"}

{block name='frontend_index_content_wrapper'}
    <form method="post" {*action="{url action=saveRegister sTarget=$sTarget sTargetAction='profile'}"*}
            class="js-register--form" id="recommendation--form" style="margin: 20px 0 20px 0">

        <div class="register--action">

            <h2 class="form-title divider">Page Product Recommendation</h2>

            <div class="select-field">
                <select id="" name="recommendation[gender]">
                    <option value="Unisex" selected="selected">Unisex</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </div>
            <div class="select-field">
                <select id="" name="recommendation[color]">
                    <option value="Unisex">red</option>
                    <option value="Male" selected="selected">white</option>
                    <option value="Female">black</option>
                    <option value="Female">blue</option>
                    <option value="Female">gray</option>
                </select>
            </div>

            <div class="select-field">
                <select id="" name="recommendation[weather]">
                    <option value="Unisex">Winter</option>
                    <option value="Male" selected="selected">Summer</option>
                    <option value="Female">lkjafd</option>
                    <option value="Female">slkdfjkldsjf</option>
                </select>
            </div>
            {*<div class="form-field-checkbox">
                <input name="sNewsletterMarketing" type="checkbox" id="sNewsletterMarketing" value="1" class='form-checkbox js--gtm-event' data-gtmDomEvent='input'  data-gtmMethod="newsletterSubscription" checked />
                <label for="sNewsletterMarketing" class="text-secondary">{s name='RegisterCheckboxSubscribeLabel' namespace='themes/satisfyer/frontend/register/index'}{/s}</label>
            </div>*}
            <button type="submit"
                    class="js-register--submit btn is--primary is--large account-submit-btn"
                    name="Submit"
                    data-preloader-button="true">Find
            </button>
        </div>
    </form>
    <div class="product-slider--container">
        {if empty($articles)}
            <h3>Filter hasn't results!</h3>
        {else}
        {include file="frontend/_includes/product_slider_items.tpl" articles=$articles}
        {/if}
    </div>
    {*<h4>This is rand value: <u>{$test}</u></h4>*}

    {*{foreach $list as $recommendation}
        <div>
            <h5>{$recommendation.name}</h5>
            <h6>{$recommendation.description}</h6>
            <h6>{$recommendation.gender}</h6>
        </div>
    {/foreach}*}
{/block}