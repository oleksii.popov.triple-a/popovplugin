<?php

namespace PopovPlugin\Repositories;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use PopovPlugin\Dto\FilterDto;
use Shopware\Models\Article\Article;
use Shopware\Models\Article\Repository;

class PageRepository extends Repository
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        $classMetadata = new ClassMetadata(Article::class);
        parent::__construct($entityManager, $classMetadata);
    }

    public function getArticleList(FilterDto $filterDto): array
    {
        return $this->_em->getConnection()->createQueryBuilder()
            ->from('s_articles', 'a')
            ->select('a.id, a.name, a.description, aa.aaa_gender')
            ->leftJoin('a', 's_articles_details', 'ad', 'a.id = ad.articleID')
            ->leftJoin('ad', 's_articles_attributes', 'aa', 'ad.id = aa.articledetailsID')
            ->where("aa.aaa_gender = :gender")
            //->andWhere("aa.aaa_gender = :color")
            ->setMaxResults($filterDto->getLimit())
            ->setParameter(':gender', $filterDto->getGender())
            //->setParameter(':color', $filterDto->getColor())
            ->execute()
            ->fetchAll();
    }
}