<?php

use PopovPlugin\Dto\FilterDto;
use PopovPlugin\Services\PageService;

class Shopware_Controllers_Frontend_PageRecommendation extends Enlight_Controller_Action
{
    /**
     * @var PageService
     */
    private $pageService;

    public function preDispatch(): void
    {
        $this->pageService = Shopware()->Container()->get('popov_service_plugin.page');
        $this->View()->addTemplateDir(__DIR__ . '/../../Resources/views/');
    }

    /**
     * @throws Exception
     */
    public function indexAction()
    {
        $limit = (int) $this->Request()->getParam('limit');
        $filterForm = $this->Request()->getPost('recommendation');
        $filterForm = array_merge($filterForm, ['limit' => $limit]);

        $filterDto = (new FilterDto())->fromArray($filterForm);
        $list = $this->pageService->getArticleList($filterDto);

        $this->View()->assign('articles', $list);
    }
}