<?php

namespace PopovPlugin\Services;

use Exception;
use PopovPlugin\Dto\FilterDto;
use PopovPlugin\Repositories\PageRepository;

class PageService
{
    /**
     * @var PageRepository
     */
    private $pageRepository;

    public function __construct(PageRepository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * @throws Exception
     */
    public function getArticleList(FilterDto $filterDto): array
    {
        return $this->pageRepository->getArticleList($filterDto);
    }
}