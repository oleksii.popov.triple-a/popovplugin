<?php

namespace PopovPlugin\Dto;

class FilterDto
{
    /**
     * @var int
     */
    private $limit = 10;

    /**
     * @var string
     */
    private $gender = 'Unisex';

    /**
     * @var string
     */
    private $color = 'White';

    /**
     * @var string
     */
    private $weather = 'Summer';

    /**
     * @param array|null $filterData
     *
     * @return $this
     */
    public function fromArray(?array $filterData): self
    {
        $this->limit = $filterData['limit'] ?? $this->limit;
        $this->limit = $this->limit <= 0 ?: $this->limit;
        $this->limit = $this->limit > 100 ?: $this->limit;

        $this->gender = $filterData['gender'] ?? $this->gender;
        $this->color = $filterData['color'] ?? $this->color;
        $this->weather = $filterData['weather'] ?? $this->weather;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     *
     * @return $this
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     *
     * @return $this
     */
    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     *
     * @return $this
     */
    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return string
     */
    public function getWeather(): string
    {
        return $this->weather;
    }

    /**
     * @param string $weather
     *
     * @return $this
     */
    public function setWeather(string $weather): self
    {
        $this->weather = $weather;

        return $this;
    }
}